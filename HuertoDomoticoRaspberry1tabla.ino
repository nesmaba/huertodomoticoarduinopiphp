#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <DHT.h>

#define DHTTYPE DHT11   // DHT 11

const int DHTPin = 5; // what digital pin we're connected to DHT11
const int sensorPinHumedad = A1; // Entrada Analógica para la lectura del sensor de Humedad
const int pinBombaAgua = 9;
const int ledPIN_red = 7;
const int ledPIN_green = 10;

const int idHuerto=2;
int humedadTierra;
int temperaturaAmb, humedadAmb;
int totalRiegos=0;

DHT dht11(DHTPin, DHTTYPE);

//Creamos el objeto lcd  dirección  0x3F, me la da iscanner, y 20 columnas x 4 filas  
LiquidCrystal_I2C lcd(0x3F, 20, 4);
 
void setup(){
   
   Serial.begin(9600); // Activamos la salida de resultado por el puerto Serie.
   // Asigno pin 9 de arduino para poder enviar señales a la bomba de agua 1 (izquierda)
   pinMode(pinBombaAgua, OUTPUT);

   // Iniciamos el sensor DHT11
   dht11.begin();
   
   // Inicializar el LCD 
   lcd.begin();
   lcd.setCursor(0,0);
   lcd.print("Temp=      Hum=   ");
}
 
void loop(){

  // Sensor de Humedad de la tierra
  // 0 - 250 seco
  // 250 - 600 húmedo
  // 600 - 950 Encharcado
  humedadTierra = analogRead(sensorPinHumedad);
  int error;
  //Serial.println(humedadTierra);
  
  // Leemos la temperatura y la humedad del ambiente del sensor DHT11
  temperaturaAmb = (int) dht11.readTemperature(); // Devuelven float pero hago cast a int para quitar decimales.
  humedadAmb = (int) dht11.readHumidity(); // Devuelven float pero hago cast a int para quitar decimales.
  //Serial.println(temperaturaAmb);
  if(isnan(temperaturaAmb) || isnan(humedadAmb)){ // Error del sensor de temperatura/humedad ambiente.
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("ERROR Temp/Hum ambiente");
  }else{
    lcd.setCursor(6,0);
    lcd.print(temperaturaAmb);
    lcd.print((char)223); // Mostramos el símbolo º
    lcd.print("C");
    lcd.setCursor(16,0);
    lcd.print(humedadAmb);
    lcd.print((char)37); // Mostramos el símbolo %
   }

  if(!isnan(humedadTierra)){
    lcd.setCursor(0,1);
    lcd.print("Humedad Tierra: ");
    lcd.print(humedadTierra);
  }
  
  // Condiciones de aviso de riego
  if(humedadTierra<=250){
    lcd.setCursor(0,2);
    lcd.print("      Me Ahogo!     ");
    //Serial.println("Me ahogo! ;(");
    //if(digitalRead(pinBombaAgua) != LOW)
    digitalWrite(pinBombaAgua, LOW);   // poner el Pin en LOW. Mando la señal de apagar la bomba de agua 
    digitalWrite(ledPIN_red , HIGH);   // poner el Pin en HIGH
    digitalWrite(ledPIN_green , LOW);   // poner el Pin en HIGH

  }else if((humedadTierra>250) && (humedadTierra<=600)){
      lcd.setCursor(0,2);
      lcd.print("   Estoy bien :)    ");
      //Serial.println("Estoy bien! :)");
      //if(digitalRead(pinBombaAgua) != LOW)
      digitalWrite(pinBombaAgua, LOW);   // poner el Pin en LOW. Mando la señal de apagar la bomba de agua 
      digitalWrite(ledPIN_red , LOW);    // poner el Pin en LOW
      digitalWrite(ledPIN_green , HIGH);   // poner el Pin en HIGH
      
  }else{  //if(humedadTierra>600){
      lcd.setCursor(0,2);
      lcd.print("   Necesito Agua!   ");
      //Serial.println("Necesito agua! :'(");
      //if(digitalRead(pinBombaAgua) != HIGH)
      digitalWrite(pinBombaAgua, HIGH);   // poner el Pin en HIGH. Mando la señal de activar la bomba de agua
      // Realizamos riego e incrementamos el valor de los riegos
      totalRiegos++;
      digitalWrite(ledPIN_green, LOW);    // poner el Pin en LOW
      digitalWrite(ledPIN_red, HIGH);   // poner el Pin en HIGH
    }

  lcd.setCursor(0,3);
  //lcd.print("    DESAFIO STEM    ");
  lcd.print("LA PURISIMA VALENCIA");
  /*
  Serial.println("Colegio La Purisima Valencia");
  Serial.println("EcoTerraX - Huerto Domotico");
  Serial.print("Temperatura Ambiente: ");
  Serial.print(temperaturaAmb);
  Serial.println("ºC");
  Serial.print("Humedad Ambiente: ");
  Serial.print(humedadAmb);
  Serial.println("%");
  Serial.println("*****************************");
  */
  // Enviamos datos a la Raspberry Pi para insertar en la BD
  enviarDatos();

  // Esperamos 3 segundos para volver a comprobar el estado del huerto.
  delay(3000);
} 

void enviarDatos(){
  Serial.print(idHuerto);Serial.print(",");
  //Serial.print(nombre);Serial.print(",");
  //Serial.print(localizacion);Serial.print(",");
  //Serial.print(descripcion);Serial.print(",");
  Serial.print(humedadTierra);Serial.print(",");
  Serial.print(temperaturaAmb);Serial.print(",");
  Serial.print(humedadAmb);Serial.print(",");
  Serial.print(totalRiegos);Serial.println(",");
}
